package gharar.rostami.shortner.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LinkEntryRepositoryTest {

    @Autowired
    LinkEntryRepository repo;

    @Test
    public void testAddSimple() {
        LinkEntry entity = new LinkEntry();
        entity.setSlang("test");
        repo.save(entity);
    }
}