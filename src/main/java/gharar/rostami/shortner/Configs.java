package gharar.rostami.shortner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Configs {
    @Value("${shortner.baseUrl}")
    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

}
