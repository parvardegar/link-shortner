package gharar.rostami.shortner.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LinkEntryRepository extends MongoRepository<LinkEntry, String> {
    LinkEntry findBySlang(String slang);
}
