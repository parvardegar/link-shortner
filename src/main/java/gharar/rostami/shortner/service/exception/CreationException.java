package gharar.rostami.shortner.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CreationException extends RuntimeException {

    private String badSourceUrl;
    private String existingSlang;
    private boolean tooShortSlang;

    public static CreationException badSourceUrl(String sourceUrl) {
        CreationException creationException = new CreationException();
        creationException.badSourceUrl = sourceUrl;
        return creationException;
    }

    public static CreationException slangExists(String slang) {
        CreationException creationException = new CreationException();
        creationException.existingSlang = slang;
        return creationException;
    }

    public static CreationException slangTooShort() {
        CreationException creationException = new CreationException();
        creationException.tooShortSlang = true;
        return creationException;
    }

    public String getBadSourceUrl() {
        return badSourceUrl;
    }

    public String getExistingSlang() {
        return existingSlang;
    }

    public boolean isTooShortSlang() {
        return tooShortSlang;
    }
}
