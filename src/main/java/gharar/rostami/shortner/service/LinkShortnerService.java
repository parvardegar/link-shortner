package gharar.rostami.shortner.service;

import gharar.rostami.shortner.Configs;
import gharar.rostami.shortner.api.dto.CreationRequestDTO;
import gharar.rostami.shortner.api.dto.CreationResponseDTO;
import gharar.rostami.shortner.domain.LinkEntry;
import gharar.rostami.shortner.domain.LinkEntryRepository;
import gharar.rostami.shortner.service.exception.CreationException;
import gharar.rostami.shortner.service.exception.LinkExpiredException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
public class LinkShortnerService {

    private static final int SLANG_LENGTH = 5;

    private final Configs configs;
    private final LinkEntryRepository repository;

    public LinkShortnerService(Configs configs, LinkEntryRepository repository) {
        this.configs = configs;
        this.repository = repository;
    }

    //TODO 404 va expire va ... check
    public String viewLink(String slang) {
        LinkEntry bySlang = repository.findBySlang(slang);

        checkExpiration(bySlang);

        String sourceUrl = getSourceUrl(bySlang);

        onClickLink(bySlang);

        return sourceUrl;
    }

    private void checkExpiration(LinkEntry bySlang) {
        if (Objects.nonNull(bySlang.getExpireAt()) && new Date().after(bySlang.getExpireAt())) {
            throw new LinkExpiredException();
        }
    }

    private String getSourceUrl(LinkEntry linkEntry) {
        if (linkEntry == null) return null;
        else return linkEntry.getSourceUrl();
    }

    //TODO: just throw an event, handle it else where
    private void onClickLink(LinkEntry linkEntry) {
        if (linkEntry != null) {
            linkEntry.incClickCount();
            repository.save(linkEntry);
        }
    }

    public CreationResponseDTO create(CreationRequestDTO dto) {
        validateCreationDTO(dto);

        LinkEntry linkEntry = convertToEntity(dto);

        LinkEntry saved = repository.save(linkEntry);

        return convertToCreationResponseDTO(saved);
    }

    //TODO: separate validator
    private void validateCreationDTO(CreationRequestDTO dto) {
        String sourceUrl = dto.getSourceUrl();

        //TODO: check if source is really a valid url
        if (!StringUtils.hasLength(sourceUrl)) { //TODO: prevent any kind of bad url not only empty ones
            throw CreationException.badSourceUrl(sourceUrl);
        }

        String slang = dto.getSlang();
        if (Objects.nonNull(slang) && slang.length() < SLANG_LENGTH) {
            throw CreationException.slangTooShort();
        }
        if (Objects.nonNull(slang) && Objects.nonNull(repository.findBySlang(slang))) {
            throw CreationException.slangExists(slang);
        }
    }

    //TODO: separate converter
    private LinkEntry convertToEntity(CreationRequestDTO dto) {
        LinkEntry linkEntry = new LinkEntry();

        linkEntry.setSourceUrl(dto.getSourceUrl());
        linkEntry.setSlang(StringUtils.hasLength(dto.getSlang())? dto.getSlang(): randomSlang());

        Date createAt = new Date();
        linkEntry.setCreateAt(createAt);

        linkEntry.setExpireAt(Objects.nonNull(dto.getExpireInSeconds())?
                calculateExpirationDate(dto.getExpireInSeconds(), createAt): null);

        return linkEntry;
    }

    private CreationResponseDTO convertToCreationResponseDTO(LinkEntry saved) {
        CreationResponseDTO creationResponseDTO = new CreationResponseDTO();
        creationResponseDTO.setUrl(configs.getBaseUrl()+"/"+saved.getSlang());

        return creationResponseDTO;
    }

    private Date calculateExpirationDate(Long expireInSeconds, Date createAt) {
        return new Date(createAt.toInstant().plus(expireInSeconds, ChronoUnit.SECONDS).toEpochMilli());
    }

    private String randomSlang() {
        String randomSlang = RandomStringUtils.randomAlphanumeric(SLANG_LENGTH);

        //TODO: not good for heavy load
        while (repository.findBySlang(randomSlang) != null) {
            randomSlang = RandomStringUtils.randomAlphanumeric(SLANG_LENGTH);
        }

        return randomSlang;
    }

}
