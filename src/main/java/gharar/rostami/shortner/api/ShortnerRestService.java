package gharar.rostami.shortner.api;

import gharar.rostami.shortner.api.dto.CreationRequestDTO;
import gharar.rostami.shortner.api.dto.CreationResponseDTO;
import gharar.rostami.shortner.service.LinkShortnerService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;


@RestController
public class ShortnerRestService {

    private final LinkShortnerService shortnerService;

    public ShortnerRestService(LinkShortnerService shortnerService) {
        this.shortnerService = shortnerService;
    }

    @GetMapping("/{slang}")
    public ResponseEntity<?> redirectToSource(@PathVariable String slang) throws URISyntaxException { //TODO: catch and throw better exception

        String sourceUrl = shortnerService.viewLink(slang);

        if (sourceUrl == null)
            return ResponseEntity.status(404).build();

        URI source = new URI(sourceUrl);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(source);
        return new ResponseEntity<>(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }

    @PostMapping("/short-link")
    public CreationResponseDTO create(@RequestBody CreationRequestDTO dto) {
        return shortnerService.create(dto);
    }

//    POST /short-link
//    body {
//        "sourceUrl": "",
//                "shortSlang: "", <NULLABLE>
//        "expireInSeconds": 3600, <NULLABLE>
//    }
//    OK -> 200 {"shortLink": ""}
//    Existing -> 409

}
