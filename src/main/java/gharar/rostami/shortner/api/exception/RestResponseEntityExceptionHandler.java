package gharar.rostami.shortner.api.exception;

import gharar.rostami.shortner.service.exception.CreationException;
import gharar.rostami.shortner.service.exception.LinkExpiredException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {CreationException.class})
    protected ResponseEntity<Object> handleCreationException(RuntimeException exception, WebRequest request) {
        CreationException ex = (CreationException) exception;

        //TODO: good message json body for exceptions
        //TODO: make it better
        String bodyOfResponse = "bad source url";
        HttpStatus httpStatus = HttpStatus.NOT_ACCEPTABLE;
        if (ex.getExistingSlang() != null) {
            bodyOfResponse = "duplicate slang";
            httpStatus = HttpStatus.CONFLICT;
        }
        if (ex.isTooShortSlang()) {
            bodyOfResponse = "too short slang";
        }

        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), httpStatus, request);
    }

    @ExceptionHandler(value = {LinkExpiredException.class})
    protected ResponseEntity<Object> handleLinkExpiredException(RuntimeException exception, WebRequest request) {
        return handleExceptionInternal(exception, "", new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}