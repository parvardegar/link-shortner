package gharar.rostami.shortner.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreationRequestDTO {
    @JsonProperty
    private String sourceUrl;
    @JsonProperty
    private String slang;
    @JsonProperty
    private Long expireInSeconds;

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getSlang() {
        return slang;
    }

    public void setSlang(String slang) {
        this.slang = slang;
    }

    public Long getExpireInSeconds() {
        return expireInSeconds;
    }

    public void setExpireInSeconds(Long expireInSeconds) {
        this.expireInSeconds = expireInSeconds;
    }
}
