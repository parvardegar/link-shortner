package gharar.rostami.shortner.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreationResponseDTO {

    @JsonProperty
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
